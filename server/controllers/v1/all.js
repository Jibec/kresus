import * as crypto from 'crypto';

import Accesses from '../../models/accesses';
import Accounts from '../../models/accounts';
import Alert from '../../models/alert';
import Budget from '../../models/budget';
import Category from '../../models/category';
import Operation from '../../models/operation';
import Config from '../../models/config';
import { ConfigGhostSettings } from '../../models/static-data';

import DefaultSettings from '../../shared/default-settings';
import { run as runMigrations } from '../../models/migrations';

import { makeLogger, KError, asyncErr, UNKNOWN_OPERATION_TYPE } from '../../helpers';
import { cleanData } from './helpers';

let log = makeLogger('controllers/all');

const ERR_MSG_LOADING_ALL = 'Error when loading all Kresus data';
const ENCRYPTION_ALGORITHM = 'aes-256-ctr';
const PASSPHRASE_VALIDATION_REGEXP = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
const ENCRYPTED_CONTENT_TAG = new Buffer('KRE');

async function getAllData(userId, isExport = false, cleanPassword = true) {
    let ret = {};
    ret.accounts = await Accounts.all(userId);
    ret.accesses = await Accesses.all(userId);

    if (cleanPassword) {
        ret.accesses.forEach(access => delete access.password);
    }

    ret.categories = await Category.all(userId);
    ret.operations = await Operation.all(userId);
    ret.settings = isExport ? await Config.allWithoutGhost(userId) : await Config.all(userId);

    if (isExport) {
        ret.budgets = await Budget.all(userId);
    }

    // Return alerts only if there is an email recipient.
    let emailRecipient = ret.settings.find(s => s.name === 'email-recipient');
    if (emailRecipient && emailRecipient.value !== DefaultSettings.get('email-recipient')) {
        ret.alerts = await Alert.all(userId);
    } else {
        ret.alerts = [];
    }

    return ret;
}

export async function all(req, res) {
    try {
        let { id: userId } = req.user;
        let ret = await getAllData(userId);
        res.status(200).json(ret);
    } catch (err) {
        err.code = ERR_MSG_LOADING_ALL;
        return asyncErr(res, err, 'when loading all data');
    }
}

function encryptData(data, passphrase) {
    let cipher = crypto.createCipher(ENCRYPTION_ALGORITHM, passphrase);
    return Buffer.concat([ENCRYPTED_CONTENT_TAG, cipher.update(data), cipher.final()]).toString(
        'base64'
    );
}

function decryptData(data, passphrase) {
    let rawData = new Buffer(data, 'base64');
    let [tag, encrypted] = [rawData.slice(0, 3), rawData.slice(3)];

    if (tag.toString() !== ENCRYPTED_CONTENT_TAG.toString()) {
        throw new KError('submitted file is not a valid kresus file', 400);
    }

    let decipher = crypto.createDecipher(ENCRYPTION_ALGORITHM, passphrase);
    return Buffer.concat([decipher.update(encrypted), decipher.final()]);
}

export async function export_(req, res) {
    try {
        let { id: userId } = req.user;
        let passphrase = null;

        if (req.body.encrypted === 'true') {
            if (typeof req.body.passphrase !== 'string') {
                throw new KError('missing parameter "passphrase"', 400);
            }

            passphrase = req.body.passphrase;

            // Check password strength
            if (!PASSPHRASE_VALIDATION_REGEXP.test(passphrase)) {
                throw new KError('submitted passphrase is too weak', 400);
            }
        }

        let ret = await getAllData(userId, /* isExport = */ true, !passphrase);

        ret = cleanData(ret);
        ret = JSON.stringify(ret, null, '   ');

        if (passphrase) {
            ret = encryptData(ret, passphrase);
            res.setHeader('Content-Type', 'text/plain');
        } else {
            res.setHeader('Content-Type', 'application/json');
        }

        res.status(200).send(ret);
    } catch (err) {
        err.code = ERR_MSG_LOADING_ALL;
        return asyncErr(res, err, 'when exporting data');
    }
}

export async function import_(req, res) {
    try {
        let { id: userId } = req.user;

        if (!req.body.all) {
            throw new KError('missing parameter "all" in the file', 400);
        }

        let world = req.body.all;
        if (req.body.encrypted) {
            if (typeof req.body.passphrase !== 'string') {
                throw new KError('missing parameter "passphrase"', 400);
            }

            world = decryptData(world, req.body.passphrase);

            try {
                world = JSON.parse(world);
            } catch (err) {
                throw new KError('Invalid json file or bad passphrase.', 400);
            }
        }

        world.accesses = world.accesses || [];
        world.accounts = world.accounts || [];
        world.alerts = world.alerts || [];
        world.budgets = world.budgets || [];
        world.categories = world.categories || [];
        world.operationtypes = world.operationtypes || [];
        world.operations = world.operations || [];

        // Importing only known settings prevents assertion errors in the client when
        // importing Kresus data in an older version of kresus.
        world.settings = world.settings.filter(s => DefaultSettings.has(s.name)) || [];

        log.info(`Importing:
            accesses:        ${world.accesses.length}
            accounts:        ${world.accounts.length}
            alerts:          ${world.alerts.length}
            budgets:         ${world.budgets.length}
            categories:      ${world.categories.length}
            operation-types: ${world.operationtypes.length}
            settings:        ${world.settings.length}
            operations:      ${world.operations.length}
        `);

        log.info('Import accesses...');
        let accessMap = {};
        for (let access of world.accesses) {
            let accessId = access.id;
            delete access.id;

            // An access without password should be disabled by default.
            if (!access.password) {
                access.enabled = false;
            }

            let created = await Accesses.create(userId, access);

            accessMap[accessId] = created.id;
        }
        log.info('Done.');

        log.info('Import accounts...');
        let accountIdToAccount = new Map();
        let accountNumberToAccount = new Map();
        for (let account of world.accounts) {
            if (typeof accessMap[account.bankAccess] === 'undefined') {
                log.warn('Ignoring orphan account:\n', account);
                continue;
            }

            let accountId = account.id;
            delete account.id;

            account.bankAccess = accessMap[account.bankAccess];
            let created = await Accounts.create(userId, account);

            accountIdToAccount.set(accountId, created.id);
            accountNumberToAccount.set(created.accountNumber, created.id);
        }
        log.info('Done.');

        log.info('Import categories...');
        let existingCategories = await Category.all(userId);
        let existingCategoriesMap = new Map();
        for (let c of existingCategories) {
            existingCategoriesMap.set(c.title, c);
        }

        let categoryMap = {};
        for (let category of world.categories) {
            let catId = category.id;
            delete category.id;
            if (existingCategoriesMap.has(category.title)) {
                let existing = existingCategoriesMap.get(category.title);
                categoryMap[catId] = existing.id;
            } else {
                let created = await Category.create(userId, category);
                categoryMap[catId] = created.id;
            }
        }
        log.info('Done.');

        log.info('Import budgets...');
        for (let budget of world.budgets) {
            budget.categoryId = categoryMap[budget.categoryId];
            await Budget.create(userId, budget);
        }
        log.info('Done.');

        // No need to import operation types.

        // importedTypesMap is used to set type to imported operations (backward compatibility)
        let importedTypes = world.operationtypes || [];
        let importedTypesMap = new Map();
        for (let type of importedTypes) {
            importedTypesMap.set(type.id.toString(), type.name);
        }
        log.info('Import operations...');
        for (let op of world.operations) {
            // Map operation to account.
            if (typeof op.accountId !== 'undefined') {
                if (!accountIdToAccount.has(op.accountId)) {
                    log.warn('Ignoring orphan operation:\n', op);
                    continue;
                }
                op.accountId = accountIdToAccount.get(op.accountId);
            } else {
                if (!accountNumberToAccount.has(op.bankAccount)) {
                    log.warn('Ignoring orphan operation:\n', op);
                    continue;
                }
                op.accountId = accountNumberToAccount.get(op.bankAccount);
            }

            // Remove bankAccount as the operation is now linked to account with accountId prop.
            delete op.bankAccount;

            let categoryId = op.categoryId;
            if (typeof categoryId !== 'undefined') {
                if (typeof categoryMap[categoryId] === 'undefined') {
                    log.warn('Unknown category, unsetting for operation:\n', op);
                }

                op.categoryId = categoryMap[categoryId];
            }

            // Set operation type base on operationId
            if (typeof op.operationTypeID !== 'undefined') {
                let key = op.operationTypeID.toString();
                if (importedTypesMap.has(key)) {
                    op.type = importedTypesMap.get(key);
                } else {
                    op.type = UNKNOWN_OPERATION_TYPE;
                }
                delete op.operationTypeID;
            }

            // Remove attachments, if there were any.
            delete op.attachments;
            delete op.binary;

            await Operation.create(userId, op);
        }
        log.info('Done.');

        log.info('Import settings...');
        let shouldResetMigration = true;
        for (let setting of world.settings) {
            if (ConfigGhostSettings.has(setting.name)) {
                continue;
            }

            if (setting.name === 'migration-version') {
                // Overwrite previous value of migration-version setting.
                let found = await Config.byName(userId, 'migration-version');
                if (found) {
                    shouldResetMigration = false;
                    log.debug(`Updating migration-version index to ${setting.value}.`);
                    await Config.update(userId, found.id, { value: setting.value });
                    continue;
                }
            }

            if (
                setting.name === 'defaultAccountId' &&
                setting.value !== DefaultSettings.get('defaultAccountId')
            ) {
                if (!accountIdToAccount.has(setting.value)) {
                    log.warn(`unknown default account id: ${setting.value}, skipping.`);
                    continue;
                }
                setting.value = accountIdToAccount.get(setting.value);

                await Config.updateByKey(userId, 'defaultAccountId', setting.value);
                continue;
            }

            // Note that former existing values are not overwritten!
            await Config.findOrCreateByName(userId, setting.name, setting.value);
        }

        if (shouldResetMigration) {
            // If no migration-version has been set, just reset
            // migration-version value to 0, to force all the migrations to be
            // run again.
            log.info(
                'The imported file did not provide a migration-version value. ' +
                    'Resetting it to 0 to run all migrations again.'
            );
            await Config.updateByKey(userId, 'migration-version', '0');
        }
        log.info('Done.');

        log.info('Import alerts...');
        for (let a of world.alerts) {
            // Map alert to account.
            if (typeof a.accountId !== 'undefined') {
                if (!accountIdToAccount.has(a.accountId)) {
                    log.warn('Ignoring orphan alert:\n', a);
                    continue;
                }
                a.accountId = accountIdToAccount.get(a.accountId);
            } else {
                if (!accountNumberToAccount.has(a.bankAccount)) {
                    log.warn('Ignoring orphan alert:\n', a);
                    continue;
                }
                a.accountId = accountNumberToAccount.get(a.bankAccount);
            }

            // Remove bankAccount as the alert is now linked to account with accountId prop.
            delete a.bankAccount;
            await Alert.create(userId, a);
        }
        log.info('Done.');

        log.info('Running migrations...');
        await runMigrations();
        log.info('Done.');

        log.info('Import finished with success!');
        res.status(200).end();
    } catch (err) {
        return asyncErr(res, err, 'when importing data');
    }
}
